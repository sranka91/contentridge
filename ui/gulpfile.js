var browserify = require('browserify'),
    gulp = require('gulp'),
    sourcemaps = require('gulp-sourcemaps'),
    sass = require('gulp-sass'),
    autoprefixer = require('gulp-autoprefixer'),
    source = require('vinyl-source-stream'),
    buffer = require('vinyl-buffer'),
    browserSync = require('browser-sync');

var entryPoint = 'src/js/index.js',
    browserDir = '',
    sassWatchPath = 'src/styles/**/*.scss',
    jsWatchPath = 'src/js/**/*.js',
    htmlWatchPath = '*.html';

/* 
 *  Transpiling ES6 to ES5 
 */
gulp.task('js', function () {
    return browserify(entryPoint, {
            debug: true,
            extensions: ['es6']
        })
        .transform("babelify", {
            presets: ["es2015"]
        })
        .bundle()
        .pipe(source('bundle.js'))
        .pipe(buffer())
        .pipe(sourcemaps.init({
            loadMaps: true
        }))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('./build/'))
        .pipe(browserSync.reload({
            stream: true
        }));
});

/* 
 *  Update browser on changes
 */
gulp.task('browser-sync', function () {
    const config = {
        server: {
            baseDir: browserDir
        }
    };

    return browserSync(config);
});

/* 
 *  Compile SCSS files
 */
gulp.task('sass', function () {
    return gulp.src(sassWatchPath)
        .pipe(sourcemaps.init())
        .pipe(sass().on('error', sass.logError))
        .pipe(autoprefixer({
            browsers: ['last 2 versions']
        }))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('./build/css'))
        .pipe(browserSync.reload({
            stream: true
        }));
});

/* 
 *  Watch Changes
 */

gulp.task('watch', function () {
    gulp.watch(jsWatchPath, ['js']);
    gulp.watch(sassWatchPath, ['sass']);
    gulp.watch(htmlWatchPath, function () {
        return gulp.src('')
            .pipe(browserSync.reload({
                stream: true
            }))
    });
});

/* 
    Add 'browser-sync' to the below list in case you want a standalone reloading server. 
    I have removed this as I am using XAMPP to use PHP templates 
*/

gulp.task('run', ['js', 'sass', 'watch']);
