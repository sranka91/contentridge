# ContentRidge UI Setup

## Outline
To enable templating, we have used PHP as the templating language. Since cross domain calls are not allowed when we use the file protocol in HTMLs, I had to setup the APACHE server environment which I have done using XAMPP. 

This also caters to handling the PHP files and includes. Node JS has been used to enable watch,SASS and ES6 compilation.

* Install XAMPP (https://www.apachefriends.org/download.html)
* Install Node JS(https://nodejs.org/en/)


## Getting Started
* Open XAMPP Control Panel and start the APACHE Server.
* Once you have XAMPP installed, check out into “htdocs/<your-custom-folder>” folder inside your XAMPP htdocs folder
(eg: C:\xampp\htdocs\<your-custom-folder>) .
* Run `npm install` to install dependencies from `package.json` in “htdocs/<your-custom-folder>”
* Run gulp run  to start compilation and watch.
* Open “http://localhost/<your-custom-folder>/”  in your browser and you are done. 