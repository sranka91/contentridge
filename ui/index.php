<!DOCTYPE html>
  <html>
    <head>

      <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <link type="text/css" rel="stylesheet" href="build/css/index.css"/>
      <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    </head>

    <body>
      <h1>Welcome to ContentRidge</h1>
      <script type="text/javascript" src="build/bundle.js"></script>
    </body>
  </html>