# ContentRidge Code Setup

Core Developers Group

* [Abhishek Patel](https://www.linkedin.com/in/abhishekpatel91/)

* [Bimmi Soi](https://www.linkedin.com/in/bimmi-soi-05569842/)

* [Kamal Pant](https://www.linkedin.com/in/kamal-pant-57b2191a/)

* [Shourya Ranka](https://www.linkedin.com/in/sranka23/)


## Notes
UI (Front End) code setup for local prototyping is in the "ui" folder. This folder shall only be used to get CSS and JS outputs / static HTML pages.

